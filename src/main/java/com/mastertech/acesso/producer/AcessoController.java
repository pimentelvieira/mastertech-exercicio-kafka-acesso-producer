package com.mastertech.acesso.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class AcessoController {

    @Autowired
    private AcessoProducer producer;

    @GetMapping("/acesso/{cliente}/{porta}")
    public void acesso(@PathVariable String cliente, @PathVariable String porta) {
        Acesso acesso = new Acesso();
        acesso.setCliente(cliente);
        acesso.setPorta(porta);
        acesso.setTemAcesso(new Random().nextBoolean());
        producer.enviarAoKafka(acesso);
    }
}

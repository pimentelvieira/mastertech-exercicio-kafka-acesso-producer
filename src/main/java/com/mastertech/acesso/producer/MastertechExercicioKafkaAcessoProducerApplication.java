package com.mastertech.acesso.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MastertechExercicioKafkaAcessoProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastertechExercicioKafkaAcessoProducerApplication.class, args);
	}
}
